﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrthoCamera : MonoBehaviour {

	public Transform targetTransf;

	void Update () 
	{
		transform.LookAt (targetTransf.position);		
	}
}
