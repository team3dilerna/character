﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerOrtho : MonoBehaviour {

	// Private
	private Animator anim;
	private float horizontal = 0.0f;
	private float vertical = 0.0f;

	private Vector3 targetPos = Vector3.zero;
	private float speed = 0.0f;
	private float acceleration = 1.0f;
	private float targetDistance = 1.0f;
	private float breakDistance = 3.0f;

	void Start () 
	{
		anim = GetComponent<Animator> ();
	}
	
	void Update()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit))
			{
				targetPos = hit.point;
			}
		}
	}

	void FixedUpdate () 
	{
		if (targetPos != Vector3.zero) 
		{
			Direction ();
			Move ();
		}
	}

	bool hasReachedTarget()
	{
		return (Vector3.Distance (targetPos, transform.position) <= targetDistance);
	}

	bool hasToBreak()
	{
		return (speed > 0.5f) && (Vector3.Distance (targetPos, transform.position) - breakDistance <= 0.0f);
	}

	void Move()
	{
		if (!hasReachedTarget() && !hasToBreak()) 
		{
			speed += acceleration * Time.deltaTime; 
			
			if (speed > 1)
				speed = 1.0f;
			
			anim.SetFloat ("Speed", speed);
		} 
		else
		{
			speed -= acceleration * Time.deltaTime;
			
			if (speed < 0)
				speed = 0.0f;
			
			anim.SetFloat ("Speed", Mathf.Abs (speed));
		}
	}

	void Direction()
	{
		Vector3 point = targetPos - transform.position;
		float angleEuler = Mathf.Rad2Deg * Mathf.Atan (point.x / point.z);
		
		// First quadrant
		if (point.x > 0 && point.z > 0)
		{
			// Do nothing
		}
		// Fourth quadrant
		else if (point.x > 0 && point.z < 0)
		{
			angleEuler += 180.0f;
		}
		// Second quadant
		else if (point.x < 0 && point.z > 0)
		{
			// Do nothing
		}
		// Third quadrant
		else if (point.x < 0 && point.z < 0)
		{
			angleEuler += 180.0f;
		}
		
		if (angleEuler != transform.rotation.eulerAngles.y) 
		{
			float diff = angleEuler - transform.rotation.eulerAngles.y;
			
			Vector3 rotation = transform.rotation.eulerAngles;
			rotation.y += diff;
			
			transform.rotation = Quaternion.Euler(rotation);
		}
	}
}
