﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller2 : MonoBehaviour {

	// Public
	public float force = 5.0f;

	// Private
	private Animator anim;
	private Rigidbody body;

	private float groundDistance = 0.1f;

	void Start () 
	{
		anim = GetComponent<Animator> ();
		body = GetComponent<Rigidbody> ();
	}

	void Update () 
	{
		float horizontal = Input.GetAxis ("Horizontal");	
		float vertical = Input.GetAxis ("Vertical");	

		Direction (horizontal, vertical);
		Move (horizontal, vertical);
	}

	void Move(float horizontal, float vertical)
	{
		if (IsGrounded() && Input.GetButtonDown ("Jump")) 
		{
			// If the character is hitting ground with his foots and user presses Jump the character will have an impulse force
			body.AddForce (Vector3.up * force, ForceMode.Impulse);
		}

		Vector3 result = new Vector3 (horizontal, 0.0f, vertical);
		anim.SetFloat ("Speed", result.magnitude);
	}

	void Direction(float horizontal, float vertical)
	{
		if (horizontal != 0.0f || vertical != 0.0f) 
		{
			Vector3 result = new Vector3 (horizontal, 0.0f, vertical);
			float angle = Vector3.Angle (result, Vector3.forward);

			if (horizontal < 0.0f) 
			{
				angle *= -1;
			}

			transform.rotation = Quaternion.Euler (Vector3.up * angle);
		}
	}

	bool IsGrounded()
	{
		Vector3 startPos = transform.position;
		Vector3 endPos = transform.position;
		endPos.y -= groundDistance;

		// A line is drawn that has the origin in "transform.position", the direction is "Vector3.down" and distance "groundDistance" in Scene Window (not in Game Window)
		Debug.DrawLine(startPos, endPos, Color.cyan); 

		// The same line that we draw previously is used (not drawn) to check the distance of the character and the ground.
		bool isCharGrounded = Physics.Raycast (transform.position, Vector3.down, groundDistance);
	
		return isCharGrounded;
	}
}
