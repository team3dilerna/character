﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// We require a component Animator
[RequireComponent(typeof(Animator))]

public class PlayerController : MonoBehaviour {

	// Public
	public float rotateSpeed = 90.0f;

	// Private
	private Animator anim;

	void Start () 
	{
		anim = GetComponent<Animator> ();		
	}
	
	void Update () 
	{
		// Use of Vertical Axis to move forward the character
		float speed = Input.GetAxis ("Vertical");
		anim.SetFloat ("Speed", speed);

		// Use of Horizontal Axis to rotate the character
		float turn = Input.GetAxis ("Horizontal");
		transform.Rotate (Vector3.up, rotateSpeed * turn * Time.deltaTime);
	}
}
