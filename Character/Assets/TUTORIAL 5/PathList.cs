﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathList 
{
	private List<Transform> m_transform;
	private int m_index;

	public PathList()
	{
		m_index = -1;
		m_transform = new List<Transform> ();
	}

	public PathList(List<Transform> path)
	{
		if (path.Count > 0) 
			m_index = 0;
		else
			m_index = -1;

		m_transform = path;
	}

	public Transform getNextStep()
	{
		Transform step = null;

		if (m_index != -1)
		{
			step = m_transform [m_index];
			m_index = (m_index + 1) % m_transform.Count;
		}	

		return step;
	}

	public void addStep (Transform transform)
	{
		m_transform.Add (transform);

		if (m_index == -1)
			m_index = 0;
	}


}
