﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIA : MonoBehaviour {

	// Public
	public Transform target1;
	public Transform target2;
	public float targetDistance = 5.0f;
	public float acceleration = 1.0f;

	// Private
	private Animator anim;
	private Transform target;
	private PathList path;

	private float speed;

	void Start () 
	{
		anim = GetComponent<Animator> ();

		path = new PathList ();
		path.addStep (target1);
		path.addStep (target2);

		target = path.getNextStep();
	}

	void Update () 
	{
	}

	void FixedUpdate ()
	{
		Direction ();
		Move ();
	}

	void Move()
	{
		if (!hasReachedTarget()) 
		{
			speed += acceleration * Time.deltaTime; 

			if (speed > 1)
				speed = 1.0f;

			anim.SetFloat ("Speed", speed);
		} 
		else
		{
			target = path.getNextStep ();

			speed -= acceleration * Time.deltaTime;

			if (speed < 0)
				speed = 0.0f;

			anim.SetFloat ("Speed", Mathf.Abs (speed));
		}
			

		Debug.Log (speed);
	}

	bool hasReachedTarget()
	{
		return (Vector3.Distance (target.position, transform.position) <= targetDistance);
	}

	void Direction()
	{
		Vector3 point = target.position - transform.position;
		float angleEuler = Mathf.Rad2Deg * Mathf.Atan (point.x / point.z);

		// First quadrant
		if (point.x > 0 && point.z > 0)
		{
			// Do nothing
		}
		// Fourth quadrant
		else if (point.x > 0 && point.z < 0)
		{
			angleEuler += 180.0f;
		}
		// Second quadant
		else if (point.x < 0 && point.z > 0)
		{
			// Do nothing
		}
		// Third quadrant
		else if (point.x < 0 && point.z < 0)
		{
			angleEuler += 180.0f;
		}

		if (angleEuler != transform.rotation.eulerAngles.y) 
		{
			float diff = angleEuler - transform.rotation.eulerAngles.y;

			Vector3 rotation = transform.rotation.eulerAngles;
			rotation.y += diff;

			transform.rotation = Quaternion.Euler(rotation);
		}
	}
		
}
