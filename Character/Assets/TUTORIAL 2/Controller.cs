﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	// Private
	private Animator anim;

	void Start () 
	{
		anim = GetComponent<Animator> ();
	}
	

	void Update () 
	{
		float horizontal = Input.GetAxis ("Horizontal");	
		float vertical = Input.GetAxis ("Vertical");	

		// Check first the Direction of the character
		Direction (horizontal, vertical);

		// Moves the character to the direction that is looking at
		Move (horizontal, vertical);
	}

	void Move(float horizontal, float vertical)
	{
		Vector3 result = new Vector3 (horizontal, 0.0f, vertical);
		anim.SetFloat ("Speed", result.magnitude);
	}

	void Direction(float horizontal, float vertical)
	{
		if (horizontal != 0.0f || vertical != 0.0f) 
		{
			// We create a new Vector3 with the horizontal and vertical params
			Vector3 result = new Vector3 (horizontal, 0.0f, vertical);

			// We get the Angle between the default direction of the character (Vector3.forward) and the new direction
			float angle = Vector3.Angle (result, Vector3.forward);

			if (horizontal < 0.0f) 
			{
				// If horizontal Axis is negative we have to put the negative angle
				angle *= -1;
			}

			// Update the rotation of the character
			transform.rotation = Quaternion.Euler (Vector3.up * angle);
		}
	}
}
